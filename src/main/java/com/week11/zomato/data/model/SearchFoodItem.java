package com.week11.zomato.data.model;

public class SearchFoodItem {
    private Integer restaurantid;
    private String restaurantname;
    private String restaurantaddress;
    private Double restaurantrating;
    private FoodItem fooditem;

    public SearchFoodItem(Integer restaurantid, String restaurantname, String restaurantaddress,
            Double restaurantrating, FoodItem fooditem) {
        this.restaurantid = restaurantid;
        this.restaurantname = restaurantname;
        this.restaurantaddress = restaurantaddress;
        this.restaurantrating = restaurantrating;
        this.fooditem = fooditem;
    }

    public Integer getRestaurantid() {
        return restaurantid;
    }

    public void setRestaurantid(Integer restaurantid) {
        this.restaurantid = restaurantid;
    }

    public String getRestaurantname() {
        return restaurantname;
    }

    public void setRestaurantname(String restaurantname) {
        this.restaurantname = restaurantname;
    }

    public String getRestaurantaddress() {
        return restaurantaddress;
    }

    public void setRestaurantaddress(String restaurantaddress) {
        this.restaurantaddress = restaurantaddress;
    }

    public FoodItem getFooditem() {
        return fooditem;
    }

    public void setFooditem(FoodItem fooditem) {
        this.fooditem = fooditem;
    }

    public Double getRestaurantrating() {
        return restaurantrating;
    }

    public void setRestaurantrating(Double restaurantrating) {
        this.restaurantrating = restaurantrating;
    }

}

package com.week11.zomato.data.repository;

import org.springframework.data.repository.CrudRepository;

import com.week11.zomato.data.model.OrderInfo;

public interface OrderInfoRepo extends CrudRepository<OrderInfo, Integer> {

}
